/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mesaya.midtermproject;

import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class MyProductService {

    private static ArrayList<MyProduct> MyProduct = new ArrayList<>();

    
    public static ArrayList<MyProduct> getProduct() {
        return MyProduct;
    }

    public static MyProduct getProduct(int index) {
        return MyProduct.get(index);
    }
    public static boolean addProduct(MyProduct myproduct) {
        MyProduct.add(myproduct);
        return true;
    }

    public static boolean clearProduct() {
        MyProduct.remove(MyProduct);
        return true;
    }

    public static boolean updateeProduct(int index, MyProduct myproduct) {
        MyProduct.set(index, myproduct);

        return true;
    }
        public static boolean Product(MyProduct myproduct) {
        MyProduct.remove(myproduct);
        return true;
    }

    public static boolean delProduct(int index) {
        MyProduct.remove(index);
        return true;
    }
    
    public static int totalAmount(){
        int sum =0;
        for(int i=0;i<MyProduct.size();i++){
            sum+=MyProduct.get(i).getAmount();
        }return sum;
    }
     public static int totalPrice() {
        int sum = 0;
        for (int i = 0; i < MyProduct.size(); i++) {
            sum += (MyProduct.get(i).getPrice() * MyProduct.get(i).getAmount());
        }
        return sum;
     }
    

}
